#!/bin/bash -e
print_usage() {
  echo """
  This script sets up two casd instances and tests that data is consistent
  performing different operations. It will test transfers to/from the remote as
  well as via the proxy.

   * proxy.sock                       * server.sock
   |                                  |
   ------> [ Proxy instance ] <--------------> [ Remote instance ]
                    |                                    |
             CACHES_DIR/proxy                    CACHES_DIR/server


  Reads the following environment variables:
  * INPUT_DIRECTORY: directory containing blobs used as input data
  * CASD_BINARY_PATH: path to the buildbox-casd binary
  * CLI_UTILS_PATH: path to the CLI utilities to stage and transfer files to
  and from CAS
  """;
}

main() {
  set -x

  if [[ -z $INPUT_DIRECTORY ]]; then
        echo 'Error: you must specify an $INPUT_DIRECTORY';
        print_usage
        exit 1
  fi

  if [[ -z $CASD_BINARY_PATH ]]; then
        echo 'Error: you must specify the path to casd ($CASD_BINARY_PATH)';
        print_usage
        exit 1
  fi

  if [[ -z $CLI_UTILS_PATH ]]; then
        echo 'Error: you must specify $CLI_UTILS_PATH';
        print_usage
        exit 1
  fi

  CLI_DOWNLOADER_PATH="$CLI_UTILS_PATH/cli_downloader"
  CLI_UPLOADER_PATH="$CLI_UTILS_PATH/cli_uploader"
  CLI_STAGER_PATH="$CLI_UTILS_PATH/cli_stager"


  launch_casd_instances
  run_integration_test
}


function run_integration_test() {
  # Uploading directory to remote:
  DIRECTORY_DIGEST=$($CLI_UPLOADER_PATH "$REMOTE_ENDPOINT" "$INPUT_DIRECTORY") || exit 1

  # Downloading from remote:
  REMOTE_OUTPUT_DIR="$DOWNLOADS_DIR/remote-output"
  mkdir "$REMOTE_OUTPUT_DIR"
  $CLI_DOWNLOADER_PATH "$REMOTE_ENDPOINT" "$DIRECTORY_DIGEST" "$REMOTE_OUTPUT_DIR" || exit 1
  directories_match "$INPUT_DIRECTORY" "$REMOTE_OUTPUT_DIR"

  # Staging from remote:
  REMOTE_STAGE_DIRECTORY="$DOWNLOADS_DIR/remote-stage"
  nohup $CLI_STAGER_PATH "$REMOTE_ENDPOINT" "$DIRECTORY_DIGEST" "$REMOTE_STAGE_DIRECTORY" &
  sleep 5  # Delay to make sure that the whole tree is staged (*)
  directories_match "$INPUT_DIRECTORY" "$REMOTE_STAGE_DIRECTORY"
  killall cli_stager

  # Staging from proxy:
  PROXY_STAGE_DIRECTORY="$DOWNLOADS_DIR/proxy-stage"
  nohup $CLI_STAGER_PATH "$PROXY_ENDPOINT" "$DIRECTORY_DIGEST" "$PROXY_STAGE_DIRECTORY" &
  sleep 10   # Delay to make sure that the whole tree is staged (*)
  directories_match "$INPUT_DIRECTORY" "$PROXY_STAGE_DIRECTORY"
  killall cli_stager

  # (*) Those values seem to currently be suitable on the GitLab CI, but are arbitrary.

  # Downloading from proxy:
  PROXY_OUTPUT_DIR="$DOWNLOADS_DIR/proxy-output"
  mkdir "$PROXY_OUTPUT_DIR"
  $CLI_DOWNLOADER_PATH "$PROXY_ENDPOINT" "$DIRECTORY_DIGEST" "$PROXY_OUTPUT_DIR" || exit 1
  directories_match "$INPUT_DIRECTORY" "$PROXY_OUTPUT_DIR"

  # The stage directories didn't originally exist, so we expect `GetTree()` to delete them:
  assert_stage_directory_deleted "$PROXY_STAGE_DIRECTORY"
  assert_stage_directory_deleted "$REMOTE_STAGE_DIRECTORY"
}


function directories_match() {
  directories_contents_match "$1" "$2"
  directories_contain_same_executable_files "$1" "$2"
}

function directories_contents_match() {
  (diff -r "$1" "$2") \
  || (echo "Contents of directories \"$1\" and \"$2\" do not match"; exit 1)
}

function directories_contain_same_executable_files() {
  (diff <(echo $(executable_files $1)) <(echo $(executable_files $2))) \
  || (echo "Executable files in directories \"$1\" and \"$2\" do not match"; exit 1)
}

function executable_files() {
  find "$1" -executable -printf "%P\n";  # (Print paths relative to $1)
}

function assert_stage_directory_deleted() {
  if [ -d $1 ]; then
   echo "$1 was not deleted after staging"
   exit 1
  fi
}

function launch_casd_instances() {
  # Endpoints:
  SOCKETS_PATH=$(mktemp -d)
  PROXY_ENDPOINT="unix:$SOCKETS_PATH/proxy.sock"
  REMOTE_ENDPOINT="unix:$SOCKETS_PATH/server.sock"

  # Cache paths:
  CACHES_DIR=$(mktemp -d)
  DOWNLOADS_DIR=$(mktemp -d)

  # Launching remote server:
  echo -n "Launching remote instance... "
  nohup $CASD_BINARY_PATH --verbose --bind="$REMOTE_ENDPOINT" "$CACHES_DIR/server" &
  sleep 5  # Delay to make sure the buildbox-casd server process is ready
  echo "done."

  # Launching proxy:
  echo -n "Launching proxy instance... "
  nohup $CASD_BINARY_PATH --verbose --bind="$PROXY_ENDPOINT" --cas-remote="$REMOTE_ENDPOINT" "$CACHES_DIR/proxy" &
  sleep 5  # Delay to make sure the buildbox-casd proxy process is ready
  echo "done."
}

main $@
