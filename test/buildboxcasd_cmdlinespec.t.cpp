/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_cmdlinespec.h>
#include <buildboxcasd_daemon.h>

#include <buildboxcommon_commandline.h>
#include <buildboxcommon_connectionoptions.h>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace testing;

// clang-format off
const char *argvTest[] = {
    "/some/path/to/some_program.tsk",
    "--instance=dev",
    // Begin ConnectionOptions parameters
    "--cas-remote=http://127.0.0.1:50011",
    "--cas-instance=cas-dev",
    "--cas-server-cert=cas-server-cert",
    "--cas-client-key=cas-client-key",
    "--cas-client-cert=cas-client-cert",
    "--cas-access-token=cas-access-token",
    "--cas-googleapi-auth=true",
    "--cas-retry-limit=10",
    "--cas-retry-delay=500",
    "--ra-remote=http://127.0.0.1:30011",
    "--ra-instance=ra-dev",
    "--ra-server-cert=ra-server-cert",
    "--ra-client-key=ra-client-key",
    "--ra-client-cert=ra-client-cert",
    "--ra-access-token=ra-access-token",
    "--ra-googleapi-auth=false",
    "--ra-retry-limit=20",
    "--ra-retry-delay=1000",
    "--ac-remote=http://127.0.0.1:40011",
    "--ac-instance=ac-dev",
    "--ac-server-cert=ac-server-cert",
    "--ac-client-key=ac-client-key",
    "--ac-client-cert=ac-client-cert",
    "--ac-access-token=ac-access-token",
    "--ac-googleapi-auth=true",
    "--ac-retry-limit=15",
    "--ac-retry-delay=750",
    // End ConnectionOptions parameters
    "--bind=127.0.0.1:50011",
    "--quota-high=64G",
    "--quota-low=48G",
    "--reserved=3G",
    "--protect-session-blobs=true",
    "--findmissingblobs-cache-ttl=30",
    "--log-level=info",
    "/path/to/cache"
};

const char *argvTest2[] = {
    "/some/path/to/some_program.tsk",
    "--instance=dev",
    "--read-only-remote",
    // Begin ConnectionOptions parameters
    "--cas-remote=http://127.0.0.1:50011",
    "--cas-instance=cas-dev",
    "--cas-server-cert=cas-server-cert",
    "--cas-client-key=cas-client-key",
    "--cas-client-cert=cas-client-cert",
    "--cas-access-token=cas-access-token",
    "--cas-googleapi-auth=true",
    "--cas-retry-limit=10",
    "--cas-retry-delay=500",
    "--ra-remote=http://127.0.0.1:30011",
    "--ra-instance=ra-dev",
    "--ra-server-cert=ra-server-cert",
    "--ra-client-key=ra-client-key",
    "--ra-client-cert=ra-client-cert",
    "--ra-access-token=ra-access-token",
    "--ra-googleapi-auth=false",
    "--ra-retry-limit=20",
    "--ra-retry-delay=1000",
    // End ConnectionOptions parameters
    "--bind=127.0.0.1:50011",
    "--quota-high=64G",
    "--quota-low=48G",
    "--reserved=3G",
    "--protect-session-blobs=true",
    "--findmissingblobs-cache-ttl=30",
    "--log-level=info",
    "/path/to/cache"
};

const char *argvTest3[] = {
    "/some/path/to/some_program.tsk",
    "--instance=dev",
    "--read-only-remote=1",
    // Begin ConnectionOptions parameters
    "--cas-remote=http://127.0.0.1:50011",
    "--cas-instance=cas-dev",
    "--cas-server-cert=cas-server-cert",
    "--cas-client-key=cas-client-key",
    "--cas-client-cert=cas-client-cert",
    "--cas-access-token=cas-access-token",
    "--cas-googleapi-auth=true",
    "--cas-retry-limit=10",
    "--cas-retry-delay=500",
    "--ra-remote=http://127.0.0.1:30011",
    "--ra-instance=ra-dev",
    "--ra-server-cert=ra-server-cert",
    "--ra-client-key=ra-client-key",
    "--ra-client-cert=ra-client-cert",
    "--ra-access-token=ra-access-token",
    "--ra-googleapi-auth=false",
    "--ra-retry-limit=20",
    "--ra-retry-delay=1000",
    "--ac-remote=http://127.0.0.1:40011",
    "--ac-instance=ac-dev",
    "--ac-server-cert=ac-server-cert",
    "--ac-client-key=ac-client-key",
    "--ac-client-cert=ac-client-cert",
    "--ac-access-token=ac-access-token",
    "--ac-googleapi-auth=true",
    "--ac-retry-limit=15",
    "--ac-retry-delay=750",
    // End ConnectionOptions parameters
    "--bind=127.0.0.1:50011",
    "--quota-high=64G",
    "--quota-low=48G",
    "--reserved=3G",
    "--protect-session-blobs=true",
    "--findmissingblobs-cache-ttl=30",
    "--log-level=info",
    "/path/to/cache"
};
// clang-format on

TEST(CmdLineSpecTest, BasicTest)
{
    buildboxcasd::CmdLineSpec spec(
        "error", buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-"),
        buildboxcommon::ConnectionOptionsCommandLine("Remote Asset", "ra-"),
        buildboxcommon::ConnectionOptionsCommandLine("Action Cache", "ac-"));
    buildboxcommon::CommandLine commandLine(spec.d_spec);

    ASSERT_TRUE(
        commandLine.parse(sizeof(argvTest) / sizeof(const char *), argvTest));

    buildboxcommon::ConnectionOptions casClient, raClient, acClient;
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "cas-", &casClient));
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "ra-", &raClient));
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "ac-", &acClient));

    // checks for casClient
    EXPECT_EQ("http://127.0.0.1:50011", casClient.d_url);
    EXPECT_EQ("cas-dev", casClient.d_instanceName);
    EXPECT_EQ("cas-server-cert", casClient.d_serverCertPath);
    EXPECT_EQ("cas-client-key", casClient.d_clientKeyPath);
    EXPECT_EQ("cas-client-cert", casClient.d_clientCertPath);
    EXPECT_EQ("cas-access-token", casClient.d_accessTokenPath);
    EXPECT_TRUE(casClient.d_useGoogleApiAuth);
    EXPECT_EQ("10", casClient.d_retryLimit);
    EXPECT_EQ("500", casClient.d_retryDelay);

    // checks for raClient
    EXPECT_EQ("http://127.0.0.1:30011", raClient.d_url);
    EXPECT_EQ("ra-dev", raClient.d_instanceName);
    EXPECT_EQ("ra-server-cert", raClient.d_serverCertPath);
    EXPECT_EQ("ra-client-key", raClient.d_clientKeyPath);
    EXPECT_EQ("ra-client-cert", raClient.d_clientCertPath);
    EXPECT_EQ("ra-access-token", raClient.d_accessTokenPath);
    EXPECT_FALSE(raClient.d_useGoogleApiAuth);
    EXPECT_EQ("20", raClient.d_retryLimit);
    EXPECT_EQ("1000", raClient.d_retryDelay);

    // checks for acClient
    EXPECT_EQ("http://127.0.0.1:40011", acClient.d_url);
    EXPECT_EQ("ac-dev", acClient.d_instanceName);
    EXPECT_EQ("ac-server-cert", acClient.d_serverCertPath);
    EXPECT_EQ("ac-client-key", acClient.d_clientKeyPath);
    EXPECT_EQ("ac-client-cert", acClient.d_clientCertPath);
    EXPECT_EQ("ac-access-token", acClient.d_accessTokenPath);
    EXPECT_TRUE(acClient.d_useGoogleApiAuth);
    EXPECT_EQ("15", acClient.d_retryLimit);
    EXPECT_EQ("750", acClient.d_retryDelay);

    // Configure the daemon and test the configuration
    buildboxcasd::Daemon daemon;
    ASSERT_TRUE(daemon.configure(commandLine, spec.d_cachePath));

    EXPECT_EQ("127.0.0.1:50011", daemon.d_bind_address);
    EXPECT_EQ(48000000000, daemon.d_quota_low);
    EXPECT_EQ(64000000000, daemon.d_quota_high);
    EXPECT_EQ(3000000000, daemon.d_reserved_space);
    EXPECT_TRUE(daemon.d_protect_session_blobs);
    EXPECT_EQ(30, daemon.d_proxy_findmissingblobs_cache_ttl_seconds);
    EXPECT_EQ(buildboxcommon::LogLevel::INFO, daemon.d_log_level);
    EXPECT_EQ("/path/to/cache", daemon.d_local_cache_path);
    EXPECT_EQ(false, daemon.d_read_only_remote);
}

TEST(CmdLineSpecTest, NoActionCacheProxy)
{
    buildboxcasd::CmdLineSpec spec(
        "error", buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-"),
        buildboxcommon::ConnectionOptionsCommandLine("Remote Asset", "ra-"),
        buildboxcommon::ConnectionOptionsCommandLine("Action Cache", "ac-"));
    buildboxcommon::CommandLine commandLine(spec.d_spec);

    ASSERT_TRUE(commandLine.parse(sizeof(argvTest2) / sizeof(const char *),
                                  argvTest2));

    buildboxcommon::ConnectionOptions casClient, raClient, acClient;
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "cas-", &casClient));
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "ra-", &raClient));
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "ac-", &acClient));

    // checks for casClient
    EXPECT_EQ("http://127.0.0.1:50011", casClient.d_url);
    EXPECT_EQ("cas-dev", casClient.d_instanceName);
    EXPECT_EQ("cas-server-cert", casClient.d_serverCertPath);
    EXPECT_EQ("cas-client-key", casClient.d_clientKeyPath);
    EXPECT_EQ("cas-client-cert", casClient.d_clientCertPath);
    EXPECT_EQ("cas-access-token", casClient.d_accessTokenPath);
    EXPECT_TRUE(casClient.d_useGoogleApiAuth);
    EXPECT_EQ("10", casClient.d_retryLimit);
    EXPECT_EQ("500", casClient.d_retryDelay);

    // checks for raClient
    EXPECT_EQ("http://127.0.0.1:30011", raClient.d_url);
    EXPECT_EQ("ra-dev", raClient.d_instanceName);
    EXPECT_EQ("ra-server-cert", raClient.d_serverCertPath);
    EXPECT_EQ("ra-client-key", raClient.d_clientKeyPath);
    EXPECT_EQ("ra-client-cert", raClient.d_clientCertPath);
    EXPECT_EQ("ra-access-token", raClient.d_accessTokenPath);
    EXPECT_FALSE(raClient.d_useGoogleApiAuth);
    EXPECT_EQ("20", raClient.d_retryLimit);
    EXPECT_EQ("1000", raClient.d_retryDelay);

    // checks for acClient
    EXPECT_EQ("", acClient.d_url);
    EXPECT_EQ("", acClient.d_instanceName);
    EXPECT_EQ("", acClient.d_serverCertPath);
    EXPECT_EQ("", acClient.d_clientKeyPath);
    EXPECT_EQ("", acClient.d_clientCertPath);
    EXPECT_EQ("", acClient.d_accessTokenPath);
    EXPECT_FALSE(acClient.d_useGoogleApiAuth);
    EXPECT_EQ("4", acClient.d_retryLimit);
    EXPECT_EQ("1000", acClient.d_retryDelay);

    // Configure the daemon and test the configuration
    buildboxcasd::Daemon daemon;
    ASSERT_TRUE(daemon.configure(commandLine, spec.d_cachePath));

    EXPECT_EQ("127.0.0.1:50011", daemon.d_bind_address);
    EXPECT_EQ(48000000000, daemon.d_quota_low);
    EXPECT_EQ(64000000000, daemon.d_quota_high);
    EXPECT_EQ(3000000000, daemon.d_reserved_space);
    EXPECT_TRUE(daemon.d_protect_session_blobs);
    EXPECT_EQ(30, daemon.d_proxy_findmissingblobs_cache_ttl_seconds);
    EXPECT_EQ(buildboxcommon::LogLevel::INFO, daemon.d_log_level);
    EXPECT_EQ("/path/to/cache", daemon.d_local_cache_path);
    EXPECT_EQ(true, daemon.d_read_only_remote);
}

TEST(CmdLineSpecTest, ReadOnlyArg)
{
    buildboxcasd::CmdLineSpec spec(
        "error", buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-"),
        buildboxcommon::ConnectionOptionsCommandLine("Remote Asset", "ra-"),
        buildboxcommon::ConnectionOptionsCommandLine("Action Cache", "ac-"));
    buildboxcommon::CommandLine commandLine(spec.d_spec);

    ASSERT_TRUE(commandLine.parse(sizeof(argvTest3) / sizeof(const char *),
                                  argvTest3));

    buildboxcommon::ConnectionOptions casClient, raClient, acClient;
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "cas-", &casClient));
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "ra-", &raClient));
    EXPECT_TRUE(buildboxcommon::ConnectionOptionsCommandLine::configureChannel(
        commandLine, "ac-", &acClient));

    // checks for casClient
    EXPECT_EQ("http://127.0.0.1:50011", casClient.d_url);
    EXPECT_EQ("cas-dev", casClient.d_instanceName);
    EXPECT_EQ("cas-server-cert", casClient.d_serverCertPath);
    EXPECT_EQ("cas-client-key", casClient.d_clientKeyPath);
    EXPECT_EQ("cas-client-cert", casClient.d_clientCertPath);
    EXPECT_EQ("cas-access-token", casClient.d_accessTokenPath);
    EXPECT_TRUE(casClient.d_useGoogleApiAuth);
    EXPECT_EQ("10", casClient.d_retryLimit);
    EXPECT_EQ("500", casClient.d_retryDelay);

    // checks for raClient
    EXPECT_EQ("http://127.0.0.1:30011", raClient.d_url);
    EXPECT_EQ("ra-dev", raClient.d_instanceName);
    EXPECT_EQ("ra-server-cert", raClient.d_serverCertPath);
    EXPECT_EQ("ra-client-key", raClient.d_clientKeyPath);
    EXPECT_EQ("ra-client-cert", raClient.d_clientCertPath);
    EXPECT_EQ("ra-access-token", raClient.d_accessTokenPath);
    EXPECT_FALSE(raClient.d_useGoogleApiAuth);
    EXPECT_EQ("20", raClient.d_retryLimit);
    EXPECT_EQ("1000", raClient.d_retryDelay);

    // checks for acClient
    EXPECT_EQ("http://127.0.0.1:40011", acClient.d_url);
    EXPECT_EQ("ac-dev", acClient.d_instanceName);
    EXPECT_EQ("ac-server-cert", acClient.d_serverCertPath);
    EXPECT_EQ("ac-client-key", acClient.d_clientKeyPath);
    EXPECT_EQ("ac-client-cert", acClient.d_clientCertPath);
    EXPECT_EQ("ac-access-token", acClient.d_accessTokenPath);
    EXPECT_TRUE(acClient.d_useGoogleApiAuth);
    EXPECT_EQ("15", acClient.d_retryLimit);
    EXPECT_EQ("750", acClient.d_retryDelay);

    // Configure the daemon and test the configuration
    buildboxcasd::Daemon daemon;
    ASSERT_TRUE(daemon.configure(commandLine, spec.d_cachePath));

    EXPECT_EQ("127.0.0.1:50011", daemon.d_bind_address);
    EXPECT_EQ(48000000000, daemon.d_quota_low);
    EXPECT_EQ(64000000000, daemon.d_quota_high);
    EXPECT_EQ(3000000000, daemon.d_reserved_space);
    EXPECT_TRUE(daemon.d_protect_session_blobs);
    EXPECT_EQ(30, daemon.d_proxy_findmissingblobs_cache_ttl_seconds);
    EXPECT_EQ(buildboxcommon::LogLevel::INFO, daemon.d_log_level);
    EXPECT_EQ("/path/to/cache", daemon.d_local_cache_path);
    EXPECT_EQ(true, daemon.d_read_only_remote);
}
