/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localacproxyinstance.h>
#include <buildboxcasd_metricnames.h>

#include <buildboxcommon_grpcclient.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_protos.h>

#include <buildboxcommonmetrics_countingmetricutil.h>
#include <buildboxcommonmetrics_countingmetricvalue.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>
#include <set>
#include <string>

using namespace buildboxcasd;
using namespace buildboxcommon;

LocalAcProxyInstance::LocalAcProxyInstance(
    std::shared_ptr<ActionStorage> storage, std::shared_ptr<CasInstance> cas,
    const buildboxcommon::ConnectionOptions &ac_endpoint)
    : AcInstance(storage, cas),
      d_remote_instance_name(ac_endpoint.d_instanceName)
{
    std::shared_ptr<buildboxcommon::GrpcClient> grpcClient =
        std::make_shared<buildboxcommon::GrpcClient>();
    grpcClient->init(ac_endpoint);

    d_ac_client = std::make_shared<buildboxcommon::RemoteExecutionClient>(
        nullptr, grpcClient);
    d_ac_client->init();

    d_local_instance = std::make_shared<LocalAcInstance>(storage, cas);
}

grpc::Status
LocalAcProxyInstance::GetActionResult(const GetActionResultRequest &request,
                                      ActionResult *result)
{
    buildboxcommon::buildboxcommonmetrics::MetricGuard<
        buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
        mt(MetricNames::TIMER_NAME_AC_GET_ACTION_RESULT);

    BUILDBOX_LOG_DEBUG("Checking ActionCache for result of ActionDigest='" +
                       buildboxcommon::toString(request.action_digest()) +
                       "'");
    bool read = d_storage->readAction(request.action_digest(), result);
    if (!read) {
        BUILDBOX_LOG_INFO("Checking proxy ActionCache for ActionDigest='" +
                          buildboxcommon::toString(request.action_digest()) +
                          "'");

        std::set<std::string> outputs;
        for (std::string s : request.inline_output_files()) {
            outputs.insert(s);
        }
        if (!d_ac_client->fetchFromActionCache(request.action_digest(),
                                               outputs, result)) {

            buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
                MetricNames::COUNTER_NAME_AC_GET_ACTION_RESULT_MISSES, 1);

            BUILDBOX_LOG_INFO(
                "Some output blob(s) missing from cache for ActionDigest='" +
                buildboxcommon::toString(request.action_digest()) + "'");

            return grpc::Status(grpc::StatusCode::NOT_FOUND,
                                "Not found in ActionCache");
        }
        // Update local cache to reduce later request count
        UpdateActionResultRequest uar_req;
        uar_req.set_instance_name(request.instance_name());
        uar_req.mutable_action_digest()->CopyFrom(request.action_digest());
        uar_req.mutable_action_result()->CopyFrom(*result);

        if (!d_local_instance->UpdateActionResult(uar_req, result).ok()) {
            BUILDBOX_LOG_INFO(
                "Local ActionCache not updated to reflect remote. Write "
                "failed for ActionDigest='" +
                buildboxcommon::toString(request.action_digest()) + "'");
        }
    }

    if (AcInstance::hasAllDigests(result)) {
        BUILDBOX_LOG_INFO("ActionResult found for ActionDigest='" +
                          buildboxcommon::toString(request.action_digest()) +
                          "'");

        buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
            MetricNames::COUNTER_NAME_AC_GET_ACTION_RESULT_HITS, 1);

        return grpc::Status::OK;
    }
    else {
        buildboxcommonmetrics::CountingMetricUtil::recordCounterMetric(
            MetricNames::COUNTER_NAME_AC_GET_ACTION_RESULT_OUTPUT_MISSES, 1);

        BUILDBOX_LOG_INFO("ActionResult not found in CAS for ActionDigest='" +
                          buildboxcommon::toString(request.action_digest()) +
                          "'");
        return grpc::Status(grpc::StatusCode::NOT_FOUND,
                            "ActionResult's output files not found in CAS");
    }
}

grpc::Status LocalAcProxyInstance::UpdateActionResult(
    const UpdateActionResultRequest &request, ActionResult *result)
{ // Since the remote cache is read-only we call the same procedure as the
  // local-only cache
    return d_local_instance->UpdateActionResult(request, result);
}
