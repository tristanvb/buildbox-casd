/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_hardlinkstager.h>

#include <buildboxcasd_metricnames.h>

#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_protos.h>
#include <buildboxcommon_timeutils.h>

#include <buildboxcommonmetrics_countingmetric.h>
#include <buildboxcommonmetrics_countingmetricutil.h>

#include <chrono>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using namespace buildboxcasd;
using namespace buildboxcommon;
using namespace build::bazel::remote::execution::v2;

HardLinkStager::HardLinkStager(LocalCas *cas_storage) : FileStager(cas_storage)
{
    d_euid = geteuid();
}

HardLinkStager::HardLinkStagedDirectory::HardLinkStagedDirectory(
    const std::string &path)
    : d_staged_path(path)
{
}

HardLinkStager::HardLinkStagedDirectory::~HardLinkStagedDirectory()
{
    unstage(d_staged_path);
}

std::unique_ptr<FileStager::StagedDirectory>
HardLinkStager::stage(const Digest &root_digest, const std::string &path,
                      const Credentials *access_credentials)
{
    const bool path_is_existing_directory =
        buildboxcommon::FileUtils::isDirectory(path.c_str());

    if (path_is_existing_directory &&
        !buildboxcommon::FileUtils::directoryIsEmpty(path.c_str())) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::invalid_argument,
                                       "Could not stage "
                                           << toString(root_digest) << " as ["
                                           << path << "] is not empty");
    }

    bool can_hardlink = true;

    if (access_credentials != nullptr) {
        if (access_credentials->uid == d_euid) {
            // If the process accessing the staged files runs as the same user
            // as buildbox-casd, it can modify the files and thus, it's not
            // safe to hardlink files without risking cache corruption.
            can_hardlink = false;
        }
    }

    if (!path_is_existing_directory) {
        createDirectory(path);
    }
    else {
        chmod(path.c_str(), 0777);
    }

    try {
        stageDirectory(root_digest, path, can_hardlink);
    }
    catch (const std::exception &e) {
        BUILDBOX_LOG_DEBUG("Error staging root digest " << root_digest
                                                        << " in [" << path
                                                        << "]: " << e.what());

        // If the staging fails, we roll back the stage directory to the
        // its status before the call:
        unstage(path);

        throw;
    }

    // This object will keep the directory staged until it's destruction:
    return std::make_unique<HardLinkStagedDirectory>(path);
}

void HardLinkStager::unstage(const std::string &path)
{
    try {
        buildboxcommon::FileUtils::clearDirectory(path.c_str());
    }
    catch (const std::system_error &e) {
        BUILDBOX_LOG_ERROR("Error unstaging " << path << ": " << e.what());
    }
}

void HardLinkStager::stageDirectory(const Digest &digest,
                                    const std::string &path, bool can_hardlink)
{
    const bool is_root_directory = true;
    stageDirectoriesRecursively(digest, is_root_directory, path, can_hardlink);
}

void HardLinkStager::stageDirectoriesRecursively(const Digest &digest,
                                                 const bool is_root_directory,
                                                 const std::string &path,
                                                 bool can_hardlink,
                                                 bool readonly)
{
    Directory directory = getDirectory(digest);

    for (const NodeProperty &property :
         directory.node_properties().properties()) {
        if (property.name() == "SubtreeReadOnly") {
            readonly = property.value() == "true";
        }
    }

    // PRE: The top level directory already exists at this point.
    if (is_root_directory) {
        if (readonly) {
            chmod(path.c_str(), directoryMode(readonly));
        }
        else if (directory.node_properties().unix_mode().value() == 0755) {
            // userchroot doesn't allow the chroot directory to be more
            // permissive than 0755. So, in order to support clients like
            // buildbox-run-userchroot, we'll look for a `unix_mode` set to
            // 0755 in the root directoy and chmod() the staged directory
            // accordingly.
            chmod(path.c_str(), 0755);
        }
    }
    else {
        createDirectory(path, readonly);
    }

    const FileDescriptor dirfd(open(path.c_str(), O_DIRECTORY | O_RDONLY));
    if (dirfd.get() < 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Could not open directory " << path);
    }

    const auto compare_files = [](const FileNode &f1, const FileNode &f2) {
        return f1.digest().hash() < f2.digest().hash();
    };

    // Sort files so that files from the same objects subdirectory in the
    // local cache will be staged together. This will improve hit rate in
    // the kernel directory entry cache.
    std::sort(directory.mutable_files()->begin(),
              directory.mutable_files()->end(), compare_files);

    for (const FileNode &file : directory.files()) {
        const std::string stage_path = path + "/" + file.name();
        stageFile(file, dirfd.get(), stage_path, can_hardlink);
    }

    for (const DirectoryNode &subdirectory : directory.directories()) {
        const std::string subdirectory_path = path + "/" + subdirectory.name();
        stageDirectoriesRecursively(subdirectory.digest(), false,
                                    subdirectory_path, can_hardlink, readonly);
    }

    for (const SymlinkNode &symlink : directory.symlinks()) {
        const std::string symlink_path = path + "/" + symlink.name();
        stageSymLink(symlink.target(), symlink_path);
    }

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(
            MetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_FILES,
            directory.files_size());

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(
            MetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_DIRECTORIES,
            directory.directories_size());

    buildboxcommon::buildboxcommonmetrics::CountingMetricUtil::
        recordCounterMetric(
            MetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_SYMLINKS,
            directory.symlinks_size());
}

void HardLinkStager::applyFileProperties(const FileNode &file,
                                         const std::string &path)
{
    if (file.node_properties().has_mtime()) {
        const auto timestamp = file.node_properties().mtime();
        const std::chrono::system_clock::time_point timepoint =
            buildboxcommon::TimeUtils::parse_timestamp(timestamp);
        buildboxcommon::FileUtils::setFileMtime(path.c_str(), timepoint);
    }
    if (file.node_properties().has_unix_mode()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Unsupported FileNode property `unix_mode`");
    }
    for (const NodeProperty &property : file.node_properties().properties()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Unsupported FileNode property "
                                           << property.name() << " with value "
                                           << property.value());
    }
}

void HardLinkStager::stageFile(const FileNode &file, int dirfd,
                               const std::string &path, bool can_hardlink)
{
    // if we require properties, we must copy instead of hardlinking
    can_hardlink &= !file.node_properties().has_mtime();

    if (can_hardlink) {
        const auto cas_path = d_cas_storage->pathAtUnchecked(file.digest());

        hardLinkAt(cas_path.d_directory_fd, cas_path.d_file_name, dirfd,
                   file.name());
    }
    else {
        const auto cas_path = d_cas_storage->pathUnchecked(file.digest());

        buildboxcommon::FileUtils::copyFile(cas_path.c_str(), path.c_str());
        // try to apply stored properties
        applyFileProperties(file, path);
    }

    if (file.is_executable()) {
        try {
            buildboxcommon::FileUtils::makeExecutable(path.c_str());
        }
        catch (const std::runtime_error &e) {
            BUILDBOX_LOG_ERROR("Could not give executable permission to "
                               << path << ": " << e.what());
            throw e;
        }
    }
}

void HardLinkStager::stageSymLink(const std::string &target,
                                  const std::string &link_path)
{
    const int link_status = symlink(target.c_str(), link_path.c_str());

    if (link_status != 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Could not create symlink " << link_path << " -> " << target);
    }
}

void HardLinkStager::hardLinkAt(const int target_dir_fd,
                                const std::string &target,
                                const int link_dir_fd,
                                const std::string &link_path)
{
    // linkat is used instead of link, so that we may leverage
    // directory file descriptors which are cached. This should
    // result in a performance improvement.
    const int link_status = linkat(target_dir_fd, target.c_str(), link_dir_fd,
                                   link_path.c_str(), 0);

    if (link_status != 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Could not create hard link " << link_path << " -> " << target);
    }
}

void HardLinkStager::createDirectory(const std::string &path, bool readonly)
{
    const mode_t mode = directoryMode(readonly);
    const auto mkdir_status = mkdir(path.c_str(), mode);
    if (mkdir_status != 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Could not create directory [" << path << "]");
    }

    // `mkdir()` takes into account the umask, which affects the permissions of
    // the created directory.
    // We explicitly set it to 0777 to allow write access by clients and reduce
    // non-determinism in executed actions, unless the directory is in a
    // read-only subtree.
    const auto chmod_status = chmod(path.c_str(), mode);
    if (chmod_status != 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Could not chmod(" << path << ", " << std::oct << mode << ")");
    }
}

Directory HardLinkStager::getDirectory(const Digest &digest)
{
    const auto blob_data = d_cas_storage->readBlob(digest);
    if (blob_data == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::invalid_argument,
            "Could not read blob with digest: " << digest);
    }

    Directory directory;
    if (directory.ParseFromString(*blob_data)) {
        return directory;
    }

    BUILDBOXCOMMON_THROW_EXCEPTION(
        std::runtime_error,
        "Could not parse Directory from blob with digest: " << digest);
}

mode_t HardLinkStager::directoryMode(const bool readonly)
{
    return readonly ? 0755 : 0777;
}
