/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_INSTANCEMANAGER_H
#define INCLUDED_BUILDBOXCASD_INSTANCEMANAGER_H

#include <map>
#include <memory>
#include <mutex>
#include <string>

#include <buildboxcasd_acinstance.h>
#include <buildboxcasd_casinstance.h>
#include <buildboxcasd_rainstance.h>

namespace buildboxcasd {

struct Instance {
    std::shared_ptr<CasInstance> casInstance;
    std::shared_ptr<RaInstance> raInstance;
    std::shared_ptr<AcInstance> acInstance;
};

class InstanceManager {
  public:
    InstanceManager();

    bool addInstance(const std::string &instance_name,
                     std::shared_ptr<CasInstance> cas_instance,
                     std::shared_ptr<RaInstance> ra_instance,
                     std::shared_ptr<AcInstance> ac_instance);

    std::shared_ptr<CasInstance>
    getCasInstance(const std::string &instance_name);

    std::shared_ptr<RaInstance>
    getRaInstance(const std::string &instance_name);

    std::shared_ptr<AcInstance>
    getAcInstance(const std::string &instance_name);

    bool empty() const { return d_instance_map.empty(); }

  private:
    std::map<std::string, Instance> d_instance_map;
    std::mutex d_instance_map_mutex;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_INSTANCEMANAGER_H
